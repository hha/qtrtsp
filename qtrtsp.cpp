#include "qtrtsp.h"
#include "ui_qtrtsp.h"

QtRTSP::QtRTSP(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::QtRTSP)
{
    ui->setupUi(this);
    mViewerH = 449;
    mViewerW = 600;
    // mViewerH = ui->openCVviewer2->geometry().height();
    // mViewerW = ui->openCVviewer2->geometry().width();
    setFixedSize(geometry().width(), geometry().height());

    QCoreApplication::setOrganizationName("EnlightxConsolidate");
    QCoreApplication::setApplicationName("QtRTSP");
    QSettings settings;

    int num_of_cam = settings.beginReadArray("Cameras");
    for (int i = 0; i < num_of_cam; i++) {
        settings.setArrayIndex(i);
        camConfig rtsp_cam;
        rtsp_cam.camName = settings.value("Name").toString();
        rtsp_cam.rtspAddr = settings.value("Address").toString();
        rtsp_cam.flipped = settings.value("Flipped").toBool();
        rtsp_cam.mirrored = settings.value("Mirrored").toBool();
        camConfigs.append(rtsp_cam);
    }
    settings.endArray();

    setupCameraViewers();
}

QtRTSP::~QtRTSP()
{
    delete ui;
}

void QtRTSP::timerEvent(QTimerEvent *event)
{
    cv::Mat image;
    for (int i = 0; i < camConfigs.size(); i++) {
        if (mCapture[i].isOpened()) {
            mCapture[i] >> image;
            if (camConfigs[i].mirrored) cv::flip (image, image, 0);
            if (camConfigs[i].flipped) cv::flip (image, image, 1);
            //ui->openCVviewer[i]->showImage(image);
        }
    }
}

//void QtRTSP::on_actionRoof_top_camera_triggered()
//{
//    cv::Mat image;
//    float mRatio;
//    int w, h;
//    if (!mCapture2.isOpened())
//        if (!mCapture2.open("rtsp://192.168.1.32:554/ch0_0.h264")) return;
//    mCapture2 >> image;

//    w = image.cols;
//    h = image.rows;
//    mRatio = (float)w/(float)h;
//    if (mRatio > ((float)mViewerW/(float)mViewerH)) {
//        w = mViewerW;
//        h = (int) ((float)w / mRatio);
//    } else {
//        h = mViewerH;
//        w = (int) (h * mRatio);
//    }
//    ui->openCVviewer2->resize(QSize::QSize(w,h));
//    startTimer(50);
//}

//void QtRTSP::on_actionEntrance_camera_triggered()
//{
//    cv::Mat image;
//    float mRatio;
//    int w, h;
//    if (!mCapture1.isOpened())
//        if (!mCapture1.open("rtsp://192.168.1.94:554/ch0_0.h264")) return;
//    mCapture1 >> image;
//    w = image.cols;
//    h = image.rows;
//    mRatio = (float)w/(float)h;
//    if (mRatio > ((float)mViewerW/(float)mViewerH)) {
//        w = mViewerW;
//        h = (int) ((float)w / mRatio);
//    } else {
//        h = mViewerH;
//        w = (int) (h * mRatio);
//    }
//    ui->openCVviewer1->resize(QSize::QSize(w,h));
//    startTimer(50);
//}

//void QtRTSP::on_entrance_fliph_toggled(bool checked)
//{
//    mEntranceFlipH = checked;
//}

//void QtRTSP::on_entrance_flipv_toggled(bool checked)
//{
//    mEntranceFlipV = checked;
//}

//void QtRTSP::on_roof_flipv_toggled(bool checked)
//{
//    mRoofFlipV = checked;
//}

//void QtRTSP::on_roof_fliph_toggled(bool checked)
//{
//    mRoofFlipH = checked;
//}



void QtRTSP::on_actionAdd_camera_triggered() {
    camConfig newCam;

    QCoreApplication::setOrganizationName("EnlightxConsolidate");
    QCoreApplication::setApplicationName("QtRTSP");
    QSettings settings;

    dlAddCam = new AddCamera (this);
    int r = dlAddCam->exec();
    if (r == 1) {
        newCam.camName = dlAddCam->getName();
        newCam.rtspAddr = dlAddCam->getAddress();
        newCam.mirrored = false;
        newCam.flipped = false;
        camConfigs.append(newCam);

        // Save setting to disk
        settings.beginWriteArray("Cameras");
        settings.setArrayIndex(camConfigs.size());
        settings.setValue("Name", newCam.camName);
        settings.setValue("Address", newCam.rtspAddr);
        settings.setValue("Flipped", newCam.flipped);
        settings.setValue("Mirrored", newCam.mirrored);
        settings.endArray();
        setupCameraViewers();
    }
    delete dlAddCam;
}
