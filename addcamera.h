#ifndef ADDCAMERA_H
#define ADDCAMERA_H

#include <QDialog>
#include <QString>

namespace Ui {
class AddCamera;
}

class AddCamera : public QDialog
{
    Q_OBJECT

public:
    explicit AddCamera(QWidget *parent = 0);
    ~AddCamera();

    QString getName();
    QString getAddress();

private:
    Ui::AddCamera *ui;
};

#endif // ADDCAMERA_H
