#ifndef CQTOPENCVVIEWERGL_H
#define CQTOPENCVVIEWERGL_H

#include <QGLWidget>
#include <QOpenGLFunctions>
#include <opencv2/core/core.hpp>

class CQtOpenCVViewerGl : public QGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT

public:
    explicit CQtOpenCVViewerGl(QWidget *parent = 0);

signals:
    void imageSizeChanged (int outW, int outH);

public slots:
    bool showImage (cv::Mat image);

protected:
    void initializeGL ();
    void paintGL ();
    void resizeGL (int width, int height);

    void updateScene ();
    void renderImage ();

private:
    bool mSceneChanged;

    QImage mRenderQtImg;
    cv::Mat mOrigImage;

    QColor mBgColor;

    int mOutH;
    int mOutW;
    float mImgRatio;

    int mPosX;
    int mPosY;
};

#endif // CQTOPENCVVIEWERGL_H
