#ifndef QTRTSP_H
#define QTRTSP_H

#include <QMainWindow>
#include <opencv2/highgui/highgui.hpp>
#include <QSettings>
#include <QString>
#include <addcamera.h>
#include <cqtopencvviewergl.h>

namespace Ui {
class QtRTSP;
}

class QtRTSP : public QMainWindow
{
    Q_OBJECT

public:
    explicit QtRTSP(QWidget *parent = 0);
    ~QtRTSP();

private slots:
//    void on_actionRoof_top_camera_triggered();

//    void on_actionEntrance_camera_triggered();

//    void on_entrance_fliph_toggled(bool checked);

//    void on_entrance_flipv_toggled(bool checked);

//    void on_roof_flipv_toggled(bool checked);

//    void on_roof_fliph_toggled(bool checked);

    void on_actionAdd_camera_triggered();

private:
    Ui::QtRTSP *ui;

    QList<cv::VideoCapture> mCapture;

    AddCamera* dlAddCam;

    struct camConfig {
        QString camName;
        QString rtspAddr;
        bool   mirrored;
        bool   flipped;
        bool   displaying;
    };

    QList<camConfig> camConfigs;

    QList<CQtOpenCVViewerGl> openCVViewer;

    bool mEntranceFlipH, mEntranceFlipV, mRoofFlipH, mRoofFlipV;
    int mViewerW, mViewerH;

protected:
    void timerEvent(QTimerEvent *event);

    void setupCameraViewers();
};

#endif // QTRTSP_H
