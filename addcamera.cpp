#include "addcamera.h"
#include "ui_addcamera.h"

AddCamera::AddCamera(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddCamera)
{
    ui->setupUi(this);
}

AddCamera::~AddCamera()
{
    delete ui;
}

QString AddCamera::getName()
{
    return ui->txtName->toPlainText();
}

QString AddCamera::getAddress()
{
    return ui->txtAddress->toPlainText();
}
