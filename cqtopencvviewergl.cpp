#include "cqtopencvviewergl.h"

#include <QOpenGLFunctions>

CQtOpenCVViewerGl::CQtOpenCVViewerGl(QWidget *parent) : QGLWidget (parent)
{
    mSceneChanged = false;

    mOutH = 0;
    mOutW = 0;
    mImgRatio = 16.0f/9.0f;

    mPosX = 0;
    mPosY = 0;
}

void CQtOpenCVViewerGl::initializeGL() {
    makeCurrent();
    initializeOpenGLFunctions();

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

void CQtOpenCVViewerGl::resizeGL(int width, int height) {
    makeCurrent();
    glViewport(0, 0, (GLint)width, (GLint)height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0, width, 0, height, 0, 1);
    glMatrixMode(GL_MODELVIEW);

    mOutH = width/mImgRatio;
    mOutH = width;

    if (mOutH > height) {
        mOutW = height * mImgRatio;
        mOutH = height;
    }

    emit imageSizeChanged(mOutW, mOutH);

    mPosX = (width - mOutW)/2;
    mPosY = (height - mOutH)/2;

    mSceneChanged = true;
    updateScene();
}

void CQtOpenCVViewerGl::updateScene() {
    if (mSceneChanged && this->isVisible()) update ();
}

void CQtOpenCVViewerGl::paintGL() {
    makeCurrent();

    if (!mSceneChanged) return;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    renderImage();
    mSceneChanged = false;
}

void CQtOpenCVViewerGl::renderImage() {
    makeCurrent ();
    glClear(GL_COLOR_BUFFER_BIT);

    if (!mRenderQtImg.isNull()) {
        glLoadIdentity();
        QImage image;
        glPushMatrix();
        {
            int imW = mRenderQtImg.width();
            int imH = mRenderQtImg.height();

            if (imW != this->size().width() && imH != this->size().height()) {
                image = mRenderQtImg.scaled (QSize(mOutW, mOutH), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
            } else image = mRenderQtImg;

            glRasterPos2i(mPosX, mPosY);

            imW = image.width();
            imH = image.height();

            glDrawPixels (imW, imH, GL_RGBA, GL_UNSIGNED_BYTE, image.bits());
        }
        glPopMatrix();
        glFlush();
    }
}

bool CQtOpenCVViewerGl::showImage(cv::Mat image) {
    image.copyTo (mOrigImage);
    mImgRatio = (float)image.cols / (float)image.rows;

    if (mOrigImage.channels() == 3) mRenderQtImg = QImage ((const unsigned char*)(mOrigImage.data),
                                                           mOrigImage.cols, mOrigImage.rows,
                                                           mOrigImage.step, QImage::Format_RGB888);
    else if (mOrigImage.channels() == 1) mRenderQtImg = QImage ((const unsigned char*)(mOrigImage.data),
                                                                mOrigImage.cols, mOrigImage.rows,
                                                                mOrigImage.step, QImage::Format_Indexed8);
    else return false;
    mSceneChanged = true;
    updateScene();
    return true;
}
