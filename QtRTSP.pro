#-------------------------------------------------
#
# Project created by QtCreator 2016-01-28T00:00:32
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtRTSP
TEMPLATE = app


SOURCES += main.cpp\
        qtrtsp.cpp \
    cqtopencvviewergl.cpp \
    addcamera.cpp

HEADERS  += qtrtsp.h \
    cqtopencvviewergl.h \
    addcamera.h

FORMS    += qtrtsp.ui \
    addcamera.ui

QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.9
INCLUDEPATH += /opt/local/include
LIBS += -L/opt/local/lib
LIBS += -lopencv_calib3d
LIBS += -lopencv_core
LIBS += -lopencv_features2d
LIBS += -lopencv_flann
LIBS += -lopencv_highgui
LIBS += -lopencv_imgcodecs
LIBS += -lopencv_imgproc
LIBS += -lopencv_ml
LIBS += -lopencv_objdetect
LIBS += -lopencv_photo
LIBS += -lopencv_shape
LIBS += -lopencv_stitching
LIBS += -lopencv_superres
LIBS += -lopencv_video
LIBS += -lopencv_videoio
LIBS += -lopencv_videostab
LIBS += -lopencv_xfeatures2d
